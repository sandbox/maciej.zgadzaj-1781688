<?php
/**
 * @file
 * commerce_contribution.features.inc
 */

/**
 * Implements hook_commerce_line_item_type_info().
 */
function commerce_contribution_commerce_line_item_type_info() {
  $line_item_types = array();

  $line_item_types['contribution'] = array(
    'type' => 'contribution',
    'name' => t('Contribution'),
    'description' => t('References a contribution.'),
    'product' => TRUE,
    'add_form_submit_value' => t('Add product'),
    'base' => 'commerce_product_line_item',
  );

  return $line_item_types;
}

/**
 * Implements hook_commerce_product_type_info().
 */
function commerce_contribution_commerce_product_type_info() {
  $product_types = array();

  $product_types['contribution'] = array(
    'type' => 'contribution',
    'name' => t('Contribution'),
    'description' => t('A product type for contributions.'),
  );

  return $product_types;
}

/**
 * Implements hook_views_api().
 */
function commerce_contribution_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function commerce_contribution_node_info() {
  $items = array(
    'contribution_display' => array(
      'name' => t('Contribution display'),
      'base' => 'node_content',
      'description' => t('Use <em>contribution displays</em> to present Add to Cart form for <em>Contribution</em> products to your customers.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
