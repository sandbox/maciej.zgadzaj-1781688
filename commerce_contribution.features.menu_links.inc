<?php
/**
 * @file
 * commerce_contribution.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function commerce_contribution_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: management:admin/commerce/contributions
  $menu_links['management:admin/commerce/contributions'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/commerce/contributions',
    'router_path' => 'admin/commerce/contributions',
    'link_title' => 'Contributions',
    'options' => array(
      'attributes' => array(
        'title' => 'Manage contributions in the store.',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'admin/commerce',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Contributions');


  return $menu_links;
}
