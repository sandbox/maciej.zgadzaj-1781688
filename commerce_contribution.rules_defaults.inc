<?php
/**
 * @file
 * commerce_contribution.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_contribution_default_rules_configuration() {
  $items = array();
  $items['rules_calculate_total_contribution_for_a_product'] = entity_import('rules_config', '{ "rules_calculate_total_contribution_for_a_product" : {
      "LABEL" : "Calculate total contribution for a product",
      "PLUGIN" : "rule",
      "TAGS" : [ "Commerce Contribution" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "commerce_line_item" : { "label" : "Commerce line item", "type" : "commerce_line_item" } },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "commerce-line-item" ], "field" : "commerce_product" } },
        { "entity_has_field" : {
            "entity" : [ "commerce-line-item:commerce-product" ],
            "field" : "field_already_contributed"
          }
        }
      ],
      "DO" : [
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "commerce-line-item:commerce-product:field-already-contributed:amount" ],
              "op" : "+",
              "input_2" : [ "commerce-line-item:commerce-unit-price:amount" ]
            },
            "PROVIDE" : { "result" : { "total_contribution" : "Total contribution" } }
          }
        },
        { "component_rules_compare_total_contribution_with_target_amount" : {
            "commerce_line_item" : [ "commerce_line_item" ],
            "total_contribution" : [ "total_contribution" ]
          }
        }
      ]
    }
  }');
  $items['rules_compare_total_contribution_with_target_amount'] = entity_import('rules_config', '{ "rules_compare_total_contribution_with_target_amount" : {
      "LABEL" : "Compare total contribution with target amount",
      "PLUGIN" : "rule",
      "TAGS" : [ "Commerce Contribution" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "commerce_line_item" : { "label" : "Commerce line item", "type" : "commerce_line_item" },
        "total_contribution" : { "label" : "Total contribution", "type" : "decimal" }
      },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "commerce-line-item" ], "field" : "commerce_product" } },
        { "entity_has_field" : {
            "entity" : [ "commerce-line-item" ],
            "field" : "field_contribution_amount"
          }
        },
        { "data_is" : {
            "data" : [ "total-contribution" ],
            "op" : "\\u003E",
            "value" : [ "commerce-line-item:commerce-product:commerce-price:amount" ]
          }
        }
      ],
      "DO" : [
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "total-contribution" ],
              "op" : "-",
              "input_2" : [ "commerce-line-item:commerce-product:commerce-price:amount" ]
            },
            "PROVIDE" : { "result" : { "overcontribution" : "Overcontribution" } }
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "commerce-line-item:field-contribution-amount:amount" ],
              "op" : "-",
              "input_2" : [ "overcontribution" ]
            },
            "PROVIDE" : { "result" : { "new_contribution_amount" : "New contribution amount" } }
          }
        },
        { "data_set" : {
            "data" : [ "commerce-line-item:field-contribution-amount:amount" ],
            "value" : [ "new-contribution-amount" ]
          }
        },
        { "data_set" : {
            "data" : [ "commerce-line-item:commerce-unit-price:amount" ],
            "value" : [ "new-contribution-amount" ]
          }
        },
        { "drupal_message" : {
            "message" : "The specified contribution amount was too high. It has been adjusted to maximum currently allowable contribution for this product.",
            "type" : "warning"
          }
        }
      ]
    }
  }');
  $items['rules_disallow_too_big_contributions'] = entity_import('rules_config', '{ "rules_disallow_too_big_contributions" : {
      "LABEL" : "Disallow too big contributions",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Commerce Contribution" ],
      "REQUIRES" : [ "rules", "commerce_cart" ],
      "ON" : [ "commerce_cart_product_add" ],
      "IF" : [
        { "entity_has_field" : { "entity" : [ "commerce-line-item" ], "field" : "commerce_product" } },
        { "data_is" : { "data" : [ "commerce-line-item:type" ], "value" : "contribution" } }
      ],
      "DO" : [
        { "LOOP" : {
            "USING" : { "list" : [ "commerce-order:commerce-line-items" ] },
            "ITEM" : { "current_line_item" : "Current line item" },
            "DO" : [
              { "component_rules_calculate_total_contribution_for_a_product" : { "commerce_line_item" : [ "current-line-item" ] } }
            ]
          }
        }
      ]
    }
  }');
  $items['rules_merge_duplicate_contribution_products'] = entity_import('rules_config', '{ "rules_merge_duplicate_contribution_products" : {
      "LABEL" : "Merge duplicate contribution products",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "-1",
      "TAGS" : [ "Commerce Contribution" ],
      "REQUIRES" : [ "rules", "commerce_order", "commerce_cart" ],
      "ON" : [ "commerce_cart_product_add" ],
      "IF" : [
        { "entity_has_field" : { "entity" : [ "commerce-line-item" ], "field" : "commerce_product" } },
        { "data_is" : { "data" : [ "commerce-line-item:type" ], "value" : "contribution" } },
        { "entity_has_field" : {
            "entity" : [ "commerce-line-item:commerce-product" ],
            "field" : "field_already_contributed"
          }
        },
        { "commerce_order_contains_product" : {
            "commerce_order" : [ "commerce-order" ],
            "product_id" : [ "commerce-line-item:commerce-product:sku" ],
            "operator" : "\\u003E=",
            "value" : "1"
          }
        }
      ],
      "DO" : [
        { "LOOP" : {
            "USING" : { "list" : [ "commerce-order:commerce-line-items" ] },
            "ITEM" : { "current_line_item" : "Current line item" },
            "DO" : [
              { "component_rules_sum_duplicate_contribution_product_amounts" : {
                  "existing_commerce_line_item" : [ "current-line-item" ],
                  "new_commerce_line_item" : [ "commerce-line-item" ]
                }
              }
            ]
          }
        }
      ]
    }
  }');
  $items['rules_merge_line_item_quantities'] = entity_import('rules_config', '{ "rules_merge_line_item_quantities" : {
      "LABEL" : "Merge line item quantities",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "-2",
      "TAGS" : [ "Commerce Contribution" ],
      "REQUIRES" : [ "rules", "commerce_cart" ],
      "ON" : [ "commerce_cart_product_add" ],
      "IF" : [
        { "entity_has_field" : { "entity" : [ "commerce-line-item" ], "field" : "commerce_product" } },
        { "data_is" : { "data" : [ "commerce-line-item:type" ], "value" : "contribution" } },
        { "data_is" : {
            "data" : [ "commerce-line-item:quantity" ],
            "op" : "\\u003E",
            "value" : "1"
          }
        }
      ],
      "DO" : [
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "commerce-line-item:field-contribution-amount:amount" ],
              "op" : "*",
              "input_2" : [ "commerce-line-item:quantity" ]
            },
            "PROVIDE" : { "result" : { "new_contribution_amount" : "New contribution amount" } }
          }
        },
        { "data_set" : {
            "data" : [ "commerce-line-item:field-contribution-amount:amount" ],
            "value" : [ "new-contribution-amount" ]
          }
        },
        { "data_set" : {
            "data" : [ "commerce-line-item:commerce-unit-price:amount" ],
            "value" : [ "new-contribution-amount" ]
          }
        },
        { "data_set" : { "data" : [ "commerce-line-item:quantity" ], "value" : "1" } },
        { "entity_save" : { "data" : [ "commerce-line-item" ], "immediate" : 1 } }
      ]
    }
  }');
  $items['rules_sum_duplicate_contribution_product_amounts'] = entity_import('rules_config', '{ "rules_sum_duplicate_contribution_product_amounts" : {
      "LABEL" : "Sum duplicate contribution product amounts",
      "PLUGIN" : "rule",
      "TAGS" : [ "Commerce Contribution" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "existing_commerce_line_item" : { "label" : "Existing commerce line item", "type" : "commerce_line_item" },
        "new_commerce_line_item" : { "label" : "New commerce line item", "type" : "commerce_line_item" }
      },
      "IF" : [
        { "entity_has_field" : {
            "entity" : [ "existing-commerce-line-item" ],
            "field" : "commerce_product"
          }
        },
        { "data_is" : {
            "data" : [ "existing-commerce-line-item:type" ],
            "value" : "contribution"
          }
        },
        { "entity_has_field" : { "entity" : [ "new-commerce-line-item" ], "field" : "commerce_product" } },
        { "entity_has_field" : {
            "entity" : [ "new-commerce-line-item" ],
            "field" : "field_contribution_amount"
          }
        },
        { "data_is" : {
            "data" : [ "new-commerce-line-item:commerce-product:sku" ],
            "value" : [ "existing-commerce-line-item:commerce-product:sku" ]
          }
        },
        { "NOT data_is" : {
            "data" : [ "new-commerce-line-item:line-item-id" ],
            "value" : [ "existing-commerce-line-item:line-item-id" ]
          }
        }
      ],
      "DO" : [
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "existing-commerce-line-item:field-contribution-amount:amount" ],
              "op" : "+",
              "input_2" : [ "new-commerce-line-item:field-contribution-amount:amount" ]
            },
            "PROVIDE" : { "result" : { "merged_contribution_amount" : "Merged contribution amount" } }
          }
        },
        { "data_set" : {
            "data" : [ "existing-commerce-line-item:field-contribution-amount:amount" ],
            "value" : [ "merged-contribution-amount" ]
          }
        },
        { "data_set" : {
            "data" : [ "existing-commerce-line-item:commerce-unit-price:amount" ],
            "value" : [ "merged-contribution-amount" ]
          }
        },
        { "list_remove" : {
            "list" : [ "existing-commerce-line-item:order:commerce-line-items" ],
            "item" : [ "new-commerce-line-item" ]
          }
        }
      ]
    }
  }');
  $items['rules_update_total_contribution_amount_for_a_product'] = entity_import('rules_config', '{ "rules_update_total_contribution_amount_for_a_product" : {
      "LABEL" : "Update total contribution amount for a product",
      "PLUGIN" : "rule",
      "TAGS" : [ "Commerce Contribution" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "current_line_item" : { "label" : "Current line item", "type" : "commerce_line_item" } },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "current-line-item" ], "field" : "commerce_product" } },
        { "data_is" : { "data" : [ "current-line-item:type" ], "value" : "contribution" } },
        { "entity_has_field" : {
            "entity" : [ "current-line-item:commerce-product" ],
            "field" : "field_already_contributed"
          }
        }
      ],
      "DO" : [
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "current-line-item:commerce-product:field-already-contributed:amount" ],
              "op" : "+",
              "input_2" : [ "current-line-item:field-contribution-amount:amount" ]
            },
            "PROVIDE" : { "result" : { "total_contribution" : "Total contribution" } }
          }
        },
        { "data_set" : {
            "data" : [ "current-line-item:commerce-product:field-already-contributed:amount" ],
            "value" : [ "total-contribution" ]
          }
        },
        { "entity_save" : { "data" : [ "current-line-item:commerce-product" ], "immediate" : 1 } }
      ]
    }
  }');
  $items['rules_update_total_contribution_amounts'] = entity_import('rules_config', '{ "rules_update_total_contribution_amounts" : {
      "LABEL" : "Update total contribution amounts from completed order",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Commerce Contribution" ],
      "REQUIRES" : [ "rules", "commerce_payment" ],
      "ON" : [ "commerce_payment_order_paid_in_full" ],
      "DO" : [
        { "LOOP" : {
            "USING" : { "list" : [ "commerce-order:commerce-line-items" ] },
            "ITEM" : { "current_line_item" : "Current line item" },
            "DO" : [
              { "component_rules_update_total_contribution_amount_for_a_product" : { "current_line_item" : [ "current_line_item" ] } }
            ]
          }
        }
      ]
    }
  }');
  $items['rules_use_contribution_amount_in_line_item'] = entity_import('rules_config', '{ "rules_use_contribution_amount_in_line_item" : {
      "LABEL" : "Use contribution amount in line item",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Commerce Contribution" ],
      "REQUIRES" : [ "rules", "commerce_line_item", "commerce_product_reference" ],
      "ON" : [ "commerce_product_calculate_sell_price" ],
      "IF" : [
        { "entity_has_field" : { "entity" : [ "commerce-line-item" ], "field" : "commerce_product" } },
        { "data_is" : { "data" : [ "commerce-line-item:type" ], "value" : "contribution" } }
      ],
      "DO" : [
        { "commerce_line_item_unit_price_amount" : {
            "commerce_line_item" : [ "commerce_line_item" ],
            "amount" : [ "commerce-line-item:field-contribution-amount:amount" ],
            "component_name" : "base_price",
            "round_mode" : "1"
          }
        }
      ]
    }
  }');
  $items['rules_limit_single_contribution_amount'] = entity_import('rules_config', '{ "rules_limit_single_contribution_amount" : {
      "LABEL" : "Limit single contribution amount",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "TAGS" : [ "Commerce Contribution" ],
      "REQUIRES" : [ "rules", "commerce_cart" ],
      "ON" : [ "commerce_cart_product_add" ],
      "IF" : [
        { "entity_has_field" : { "entity" : [ "commerce-line-item" ], "field" : "commerce_product" } },
        { "data_is" : { "data" : [ "commerce-line-item:type" ], "value" : "contribution" } },
        { "data_is" : {
            "data" : [ "commerce-line-item:field-contribution-amount:amount" ],
            "op" : "\u003E",
            "value" : "30000"
          }
        }
      ],
      "DO" : [
        { "data_set" : {
            "data" : [ "commerce-line-item:field-contribution-amount:amount" ],
            "value" : "30000"
          }
        },
        { "drupal_message" : {
            "message" : "You can\u0027t contribute more that 300. Your contribution amount has been adjusted.",
            "type" : "warning"
          }
        }
      ]
    }
  }');
  return $items;
}
